# Solar Controller

This application can talk to a [Solar Assistant](https://solar-assistant.io) instance and adjust the time of use table based on tommorows weather forecast. You can set as many time-of-use tables as you want for different cloud coverage thresholds.

Solar Controller can be installed on the same raspberry pi as Solar Assistant in **under 5 minutes**!


<p align="center">
  <img src="https://solar-controller.kofoednielsen.com/screenshot.png">
</p>

Check out a live demo of the web interface here

[![Demo](/imgs/demo.svg)](https://solar-controller.kofoednielsen.com/demo.html)

> Currently the project has only been tested with Deye inverter


Check out the installation wizard here

[![Install](/imgs/install.svg)](https://solar-controller.kofoednielsen.com)

After using the installation wizard you can access your Solar Controller instance at [http://solar-assitant.local:8080](http://solar-assitant.local:8080) or `http://<pi_ip_address>:8080`


# Issues / Feature requests

If you have any issue or features request please let me know here [create issue](https://gitlab.com/kofoednielsen/solar-controller/-/issues)

# License


Copyright (C) 2023 Asbjørn Kofoed-Nielsen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

