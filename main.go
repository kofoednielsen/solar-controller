package main

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/robfig/cron/v3"
)

var (
	Port           = 8080
	Version string = "n/a"
)

//go:embed frontend/build/index.html
var indexPage []byte

func index(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(200)
	w.Write(indexPage)
}

type CloudCoverageForecastEntry struct {
	Date          time.Time `json:"date"`
	CloudCoverage float64   `json:"cloud_coverage"`
}

type CloudCoverageForecast struct {
	Days []CloudCoverageForecastEntry `json:"days"`
}

func cloudCoverageForecast(w http.ResponseWriter, req *http.Request) {
	cloudCoverageForecast := CloudCoverageForecast{}
	now := time.Now()
	for _, i := range []int{1, 2} {
		date := now.AddDate(0, 0, i)
		entry := CloudCoverageForecastEntry{
			Date:          date,
			CloudCoverage: calculateCloudAverage(date),
		}
		cloudCoverageForecast.Days = append(cloudCoverageForecast.Days, entry)
	}
	json, err := json.Marshal(cloudCoverageForecast)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Add("content-type", "application/json")
	w.Write(json)
}

func inverterConnected(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(200)
	err := inverterConnect()
	inverterDisconnect(0)
	if err == nil {
		io.WriteString(w, "1")
	} else {
		io.WriteString(w, "0")
	}
}

func load(w http.ResponseWriter, req *http.Request) {
	state := State{}
	state.Load()
	json, err := json.Marshal(state)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Add("content-type", "application/json")
	w.Write(json)
}

func save(w http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Fatal(err)
	}
	state := State{}
	err = json.Unmarshal(body, &state)
	state.Save()
	json, err := json.Marshal(state)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Add("content-type", "application/json")
	w.Write(json)
}

func daily() {
	state := State{}
	state.Load()
	if !state.Auto || state.Lat == 0 || state.Lon == 0 {
		return
	}
	now := time.Now()
	todayCloudCoverage := calculateCloudAverage(now)
	fmt.Printf("Today's cloud coverage is: %f\n", todayCloudCoverage)
	err := inverterConnect()
	if err != nil {
		log.Fatal(err)
	}
	for _, timeOfUseTable := range reverse(state.TimeOfUseTables) {
		if todayCloudCoverage >= timeOfUseTable.Threshold {
			for _, slot := range timeOfUseTable.Slots {
				parameter_name := ""
				if state.BatterySOCMode {
					parameter_name = "capacity"
				} else {
					parameter_name = "voltage"
				}
				fmt.Printf("Slot %d: %s: %f, Grid Charge: %s\n", slot.Point, parameter_name, slot.Voltage, strconv.FormatBool(slot.GridCharge))
				setInverterTimeOfUse(parameter_name, slot.Point, fmt.Sprintf("%f", slot.Voltage))
				setInverterTimeOfUse("grid_charge", slot.Point, strconv.FormatBool((slot.GridCharge)))
			}
			break
		}
	}
	inverterDisconnect(10000)
}

func main() {
	c := cron.New()
	c.AddFunc("10 0 * * *", daily)
	c.Start()
	port := 8080
	fmt.Println(fmt.Sprintf("Solar Controller %s is now running on :%d", Version, Port))
	http.HandleFunc("/", index)
	http.HandleFunc("/load", load)
	http.HandleFunc("/save", save)
	http.HandleFunc("/inverter_connected", inverterConnected)
	http.HandleFunc("/forecast", cloudCoverageForecast)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
