import os
import stat
import shutil
import re

service_dst = '/etc/systemd/system/solar-controller.service'
symlink_dst = '/etc/systemd/system/multi-user.target.wants/solar-controller.service'

#install

# move files to the root system
shutil.copy('/boot/solar-controller.service', service_dst)
if not os.path.isfile(symlink_dst):
    os.symlink(service_dst, symlink_dst)
shutil.copy('/boot/solar-controller', '/bin/solar-controller')
# make solar-controller binary executable
st = os.stat('/bin/solar-controller')
os.chmod('/bin/solar-controller', st.st_mode | stat.S_IEXEC)
os.makedirs('/var/lib/solar-controller/', exist_ok=True)

# cleanup

# remove init= from cmdline.txt
with open("/boot/cmdline.txt", "r+") as f:
    contents = f.read()
    f.seek(0)
    f.write(re.sub(r' init.+', '', contents))
    f.truncate()
os.remove('/boot/solar-controller.service')
os.remove('/boot/solar-controller')
os.remove('/boot/init.py')
