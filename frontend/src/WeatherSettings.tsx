import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";

const WeatherSettings = ({
  updateLat,
  updateLon,
  lat,
  lon,
}: {
  updateLat: (value: number | undefined) => void;
  updateLon: (value: number | undefined) => void;
  lat: number | undefined;
  lon: number | undefined;
}) => {
  const [latStr, setLatStr] = useState<string>(lat?.toString() || "");
  const [lonStr, setLonStr] = useState<string>(lon?.toString() || "");

  useEffect(() => {
    if (
      latStr === "" ||
      latStr === "0" ||
      Number(latStr) > 90 ||
      Number(latStr) < -90
    )
      updateLat(undefined);
    else updateLat(Number(latStr));
  }, [latStr, updateLat]);

  useEffect(() => {
    if (
      lonStr === "" ||
      lonStr === "0" ||
      Number(lonStr) > 180 ||
      Number(lonStr) < -180
    )
      updateLon(undefined);
    else updateLon(Number(lonStr));
  }, [lonStr, updateLon]);

  return (
    <>
      <Grid item>
        <TextField
          type="number"
          label="Latitude"
          error={lat === undefined}
          InputLabelProps={{ shrink: true }}
          style={{ width: "25ch" }}
          value={latStr}
          onChange={(e) => setLatStr(e.target.value)}
        />
      </Grid>
      <Grid item>
        <TextField
          type="number"
          label="Longitude"
          error={lon === undefined}
          InputLabelProps={{ shrink: true }}
          value={lonStr}
          style={{ width: "25ch" }}
          onChange={(e) => setLonStr(e.target.value)}
        />
      </Grid>
    </>
  );
};

export default WeatherSettings;
