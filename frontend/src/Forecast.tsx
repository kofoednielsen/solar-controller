import React from "react";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import CloudIcon from '@mui/icons-material/CloudQueue';

const toWeekday = (date: Date) =>
  date.toLocaleDateString("en-EN", { weekday: "short" });

const Day = ({ day }: { day: ForecastEntryType }) => {
  return (
    <Grid xs={6} item>
      <Card variant="outlined">
        <Grid p={1} textAlign="center" container>
          <Grid xs={12} item>
              {toWeekday(new Date(day.date))}
          </Grid>
          <Grid xs={12} item>
            <CloudIcon sx={{ fontSize: 40 }} />
          </Grid>
          <Grid xs={12} item>
            {day.cloud_coverage.toFixed()}%
          </Grid>
        </Grid>
      </Card>
    </Grid>
  );
};

type ForecastEntryType = {
  date: string;
  cloud_coverage: number;
};

export type ForecastType = {
  days: Array<ForecastEntryType>;
};

const Forecast = ({ forecast }: { forecast: ForecastType }) => {
  if (!forecast) return null;
  return (
    <>
    {forecast.days.map((d) => (
      <Day day={d} key={d.date} />
    ))}
    </>
  );
};

export default Forecast;
