import React, { useState, useEffect, useCallback } from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Alert from "@mui/material/Alert";
import Grid from "@mui/material/Grid";
import Chip from "@mui/material/Chip";
import Typography from "@mui/material/Typography";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import Button from "@mui/material/Button";
import TimeOfUse, { TimeOfUseType } from "./TimeOfUse";
import WeatherSettings from "./WeatherSettings";
import { v4 as uuidv4 } from "uuid";
import Forecast, { ForecastType } from "./Forecast";

const App = () => {
  const [lat, setLat] = useState<number>();
  const [lon, setLon] = useState<number>();
  const [auto, setAuto] = useState<boolean>();
  const [batterySOCMode, setBatterySOCMode] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [NetworkError, setNeworkError] = useState<boolean>(false);
  const [inverterConnected, setInverterConnected] = useState<boolean>(true);
  const [lastKnownServerState, setLastKnownServerState] = useState<{
    lat: number | undefined;
    lon: number | undefined;
    timeOfUseTables: Array<TimeOfUseType>;
    auto: boolean | undefined;
    batterySOCMode: boolean | undefined;
  }>();
  const [timeOfUseTables, setTimeOfUseTables] = useState<Array<TimeOfUseType>>(
    []
  );
  const [forecast, setForecast] = useState<ForecastType>();

  const saved =
    JSON.stringify(lastKnownServerState) ===
    JSON.stringify({
      lat,
      lon,
      timeOfUseTables: timeOfUseTables,
      auto,
      batterySOCMode,
    });

  useEffect(() => {
    if (lat && lon && saved) {
      if (process.env.REACT_APP_DEMO) {
        setForecast({
          days: [1, 2].map((i) => {
            let date = new Date();
            date.setTime(date.getTime() + i * 86400000);
            return {
              date: date.toISOString(),
              cloud_coverage: i * 26,
            };
          }),
        });
        return;
      }
      fetch("/forecast")
        .then((r) => r.json())
        .then((data) => {
          setForecast(data);
        });
    }
  }, [lat, lon, lastKnownServerState, saved]);

  const emptyTimeOfUseTable = useCallback(
    (threshold?: number): TimeOfUseType => ({
      id: uuidv4(),
      slots: [1, 2, 3, 4, 5, 6].map((p) => ({
        point: p,
        voltage: batterySOCMode ? 0 : 48,
        grid_charge: true,
      })),
      threshold: threshold,
    }),
    [batterySOCMode]
  );

  const checkInverterConnection = useCallback(() => {
    fetch("inverter_connected")
      .then((r) => r.text())
      .then((t) => {
        const connected = t === "1";
        setInverterConnected(connected);
        if (!connected) {
          setTimeout(checkInverterConnection, 1000);
        }
      });
  }, []);

  useEffect(() => {
    if (!loading) {
      // Only run once
      return;
    }
    if (process.env.REACT_APP_DEMO) {
      const demoTables = [emptyTimeOfUseTable(0)];
      const demoLat = 11.4321;
      const demoLon = 55.1234;
      setTimeOfUseTables(demoTables);
      setLastKnownServerState({
        lat: demoLat,
        lon: demoLon,
        timeOfUseTables: demoTables,
        auto: true,
        batterySOCMode: false,
      });
      setAuto(true);
      setBatterySOCMode(false);
      setLat(demoLat);
      setLon(demoLon);
      setLoading(false);
      setInverterConnected(true);
      return;
    }
    checkInverterConnection();
    fetch("/load")
      .then((r) => r.json())
      .then((data) => {
        setAuto(data.auto);
        setBatterySOCMode(data.batterySOCMode);
        if (data.lat !== 0) setLat(data.lat);
        if (data.lon !== 0) setLon(data.lon);
        if (data.timeOfUseTables?.length > 0) {
          setTimeOfUseTables(data.timeOfUseTables);
          setLastKnownServerState(data);
        } else {
          setTimeOfUseTables([emptyTimeOfUseTable(0)]);
          setAuto(true);
        }
        setLoading(false);
      })
      .catch((e) => {
        setNeworkError(true);
        setLoading(false);
      });
  }, [checkInverterConnection, emptyTimeOfUseTable, loading]);

  const timeOfUseSort = useCallback((tables: Array<TimeOfUseType>) => {
    const undefToMaxValue = (val: number | undefined) =>
      val === undefined ? Number.MAX_VALUE : Number(val);
    return tables
      .sort()
      .sort(
        (a, b) =>
          undefToMaxValue(a.threshold) - undefToMaxValue(b.threshold) ||
          b.id.localeCompare(a.id)
      );
  }, []);

  const updateThreshold = useCallback(
    (id: string, threshold: number | undefined) => {
      setTimeOfUseTables((timeOfUseTables) => {
        const old = timeOfUseTables.find((t) => t.id === id);
        if (!old) return [];
        const others = timeOfUseTables.filter((t) => t.id !== id);
        if (threshold && others.map((o) => o.threshold).includes(threshold))
          return timeOfUseTables;

        const updated = {
          id,
          slots: old.slots,
          threshold,
        };
        return timeOfUseSort([...others, updated]);
      });
    },
    [setTimeOfUseTables, timeOfUseSort]
  );

  const updateVoltage = useCallback(
    (id: string, point: number, voltage: number | undefined) => {
      setTimeOfUseTables((timeOfUseTables) => {
        const old = timeOfUseTables.find((t) => t.id === id);
        if (!old) return [];
        const updated = {
          id,
          slots: [
            ...old.slots.filter((s) => s.point !== point),
            {
              point,
              voltage,
              grid_charge:
                old.slots.find((s) => s.point === point)?.grid_charge || false,
            },
          ].sort((a, b) => a.point - b.point),
          threshold: old.threshold,
        };
        const others = timeOfUseTables.filter((t) => t.id !== id);
        return timeOfUseSort([...others, updated]);
      });
    },
    [setTimeOfUseTables, timeOfUseSort]
  );

  const updateGridCharge = (
    id: string,
    point: number,
    grid_charge: boolean
  ) => {
    setTimeOfUseTables((timeOfUseTables) => {
      const old = timeOfUseTables.find((t) => t.id === id);
      if (!old) return [];
      const updated = {
        id,
        slots: [
          ...old.slots.filter((s) => s.point !== point),
          {
            point,
            voltage: old.slots.find((s) => s.point === point)?.voltage,
            grid_charge,
          },
        ].sort((a, b) => a.point - b.point),
        threshold: old.threshold,
      };
      const others = timeOfUseTables.filter((t) => t.id !== id);
      return timeOfUseSort([...others, updated]);
    });
  };

  const remove = (id: string | undefined) => {
    setTimeOfUseTables(timeOfUseTables.filter((t) => t.id !== id));
  };

  const inputError =
    lat === undefined ||
    lon === undefined ||
    timeOfUseTables.some(
      (t) =>
        t.threshold === undefined ||
        t.slots.some((s) => s.voltage === undefined)
    );

  const save = async () => {
    if (process.env.REACT_APP_DEMO) {
      setLastKnownServerState({
        lat,
        lon,
        timeOfUseTables,
        auto,
        batterySOCMode,
      });
      return;
    }
    await fetch("/save", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({ lat, lon, timeOfUseTables, auto, batterySOCMode }),
    })
      .then((r) => r.json())
      .then((data) => {
        setLastKnownServerState(data);
      });
  };

  const add = () => {
    setTimeOfUseTables(
      timeOfUseSort([...timeOfUseTables, emptyTimeOfUseTable()])
    );
  };

  if (NetworkError)
    return (
      <Box p={3}>
        <p>Network error, sorry 😥</p>
      </Box>
    );
  if (loading) return null;
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid justifyContent="center" container>
        <Grid xs={12} lg={8} m={2} mt={4} item>
          <Grid spacing={2} container>
            <Grid xs={12} mx={2} item>
              <Typography variant="h5" gutterBottom>
                Solar Controller <Chip label={process.env.REACT_APP_VERSION} />
                {process.env.REACT_APP_DEMO ? (
                  <Chip style={{ marginLeft: 5 }} label="demo" />
                ) : null}
                <Chip
                  variant="outlined"
                  style={{ marginLeft: 5 }}
                  color={inverterConnected ? "success" : "error"}
                  label={
                    inverterConnected
                      ? "Inverter connected"
                      : "Inverter disconnected"
                  }
                />
              </Typography>
              {inverterConnected ? null : (
                <Alert severity="error" variant="outlined">
                  No access to inverter. Please check and make sure you
                  completed step 1 of the{" "}
                  <a href="https://solar-controller.kofoednielsen.com">
                    installation guide
                  </a>
                </Alert>
              )}
            </Grid>
            <Grid xs={12} item>
              <Card variant="outlined">
                <Box p={3}>
                  <Typography pb={1} variant="h5" gutterBottom>
                    Weather
                  </Typography>
                  <Grid justifyContent="space-between" spacing={2} container>
                    <Grid item>
                      <Grid spacing={2} container>
                        <WeatherSettings
                          lat={lat}
                          lon={lon}
                          updateLat={setLat}
                          updateLon={setLon}
                        />
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Grid spacing={2} container>
                        {forecast ? <Forecast forecast={forecast} /> : null}
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              </Card>
            </Grid>
            <Grid xs={12} item>
              <Card variant="outlined">
                <Box p={3}>
                  <Grid container>
                    <Grid xs={12} item>
                      <Typography variant="h5" gutterBottom>
                        Time of use
                      </Typography>
                    </Grid>
                    <Grid xs={12} item>
                      <table>
                        <tbody>
                          <tr>
                            <td>Automatic Control</td>
                            <td>
                              <FormControlLabel
                                onClick={(e) => setAuto((a) => !a)}
                                style={{ marginLeft: 5 }}
                                control={<Switch checked={auto} />}
                                label={auto ? "Enabled" : "Disabled"}
                              />
                            </td>
                          </tr>
                          <tr>
                            <td>Battery Mode</td>
                            <td>
                              <FormControlLabel
                                onClick={(e) => setBatterySOCMode((a) => !a)}
                                style={{ marginLeft: 5 }}
                                control={<Switch checked={batterySOCMode} />}
                                label={batterySOCMode ? "SOC" : "Voltage"}
                              />
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Grid>
                  </Grid>
                  <Grid pt={1} spacing={2} container>
                    {timeOfUseTables.map((t) => (
                      <TimeOfUse
                        batterySOCMode={batterySOCMode}
                        updateThreshold={updateThreshold}
                        updateVoltage={updateVoltage}
                        updateGridCharge={updateGridCharge}
                        remove={remove}
                        timeOfUse={t}
                        key={t.id}
                      />
                    ))}
                    <Grid xs={12} md={6} lg={4} xl={3} item>
                      <Button
                        onClick={add}
                        style={{ width: "100%" }}
                        size="large"
                        variant="outlined"
                      >
                        Add
                      </Button>
                    </Grid>
                  </Grid>
                </Box>
              </Card>
            </Grid>
            <Grid xs={12} md={4} lg={2} mb={1} item>
              <Button
                fullWidth
                disabled={inputError || saved}
                onClick={save}
                size="large"
                variant="contained"
              >
                {saved ? "Saved" : "Save"}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export default App;
