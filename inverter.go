package main

import (
	"fmt"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var f mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("TOPIC: %s\n", msg.Topic())
	fmt.Printf("MSG: %s\n", msg.Payload())
}

var client mqtt.Client

func inverterConnect() error {
	opts := mqtt.NewClientOptions().AddBroker("tcp://localhost:1883").SetClientID("solar-controller")
	client = mqtt.NewClient(opts)
	token := client.Connect()
	token.Wait()
	return token.Error()
}

func inverterDisconnect(ms uint) {
	client.Disconnect(ms)
}

func setInverterTimeOfUse(category string, point int, value string) {
	token := client.Publish(fmt.Sprintf("solar_assistant/inverter_1/%s_point_%d/set", category, point), 0, false, value)
	token.Wait()
	time.Sleep(10 * time.Second)
}
